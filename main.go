package main

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/fvbock/endless"
	"github.com/rakyll/statik/fs"
	"gitlab.com/tbalsys/go-mon/server"

	_ "gitlab.com/tbalsys/go-mon/statik"
)

//go:generate statik -f -src=./ui/dist

func homeHandler(fs http.FileSystem) http.HandlerFunc {
	filename := "/index.html"

	file, err := fs.Open(filename)
	if err != nil {
		log.Fatalf("Error opening file '%s': %v", filename, err)
	}

	contents, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	homeTemplate, err := template.New("home").Parse(string(contents))

	return func(w http.ResponseWriter, r *http.Request) {
		homeTemplate.Execute(w, "ws://"+r.Host+"/stats")
	}
}

func jsonHandler(getData func() (interface{}, error)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		stats, err := getData()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(stats)
	}
}

func main() {
	statik, err := fs.New()
	if err != nil {
		log.Fatalf("Missing static content: %v", err)
	}

	http.HandleFunc("/ping", jsonHandler(server.Ping))
	http.HandleFunc("/cpu", jsonHandler(server.GetCPUStats))
	http.HandleFunc("/memory", jsonHandler(server.GetMemoryStats))
	http.HandleFunc("/disk", jsonHandler(server.GetDiskStats))
	http.HandleFunc("/stats", server.WsHandler)
	http.HandleFunc("/", homeHandler(statik))
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(statik)))

	err = endless.ListenAndServe(":80", nil)
	if err != nil && !strings.Contains(err.Error(), "use of closed network connection") {
		log.Fatal("Error: " + err.Error())
	}
	log.Println("Server stopped")
}
