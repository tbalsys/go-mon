module gitlab.com/tbalsys/go-mon

go 1.15

require (
	github.com/fvbock/endless v0.0.0-20170109170031-447134032cb6
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gorilla/websocket v1.4.2
	github.com/mackerelio/go-osstat v0.1.0
	github.com/rakyll/statik v0.1.7
	github.com/ricochet2200/go-disk-usage v0.0.0-20150921141558-f0d1b743428f
)
