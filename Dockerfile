FROM fedora:33 AS development
RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo -o /etc/yum.repos.d/yarn.repo && \
  dnf -y install golang nodejs yarn gcc-c++ make dumb-init dropbear git && \
  mkdir -p /root/.ssh/ /root/code && \
  echo "gitlab.com,172.65.251.78 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" > /root/.ssh/known_hosts && \
  echo "export PATH=\$PATH:$(go env GOPATH)/bin" >> /root/.bash_profile && \
  echo "cd code" >> /root/.bash_profile

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["bash", "-c", \
  "if [ -n \"$SSH_PRIVATE_KEY\" ]; then echo \"$SSH_PRIVATE_KEY\" > /root/.ssh/id_rsa; chmod 0600 /root/.ssh/id_rsa; fi && \
  if [ -n \"$SSH_PUBLIC_KEY\" ]; then echo \"$SSH_PUBLIC_KEY\" > /root/.ssh/authorized_keys; fi && \
  if [ -n \"$GIT_NAME\" ]; then git config --global user.name \"$GIT_NAME\"; fi && \
  if [ -n \"$GIT_EMAIL\" ]; then git config --global user.email \"$GIT_EMAIL\"; fi && \
  exec dropbear -E -F -p 127.0.0.1:22 -R -s"]

FROM development AS builder
WORKDIR /code
ENV PATH=$PATH:/root/go/bin
ENV CGO_ENABLED=0

COPY ui/package.json ui/yarn.lock ui/
RUN pushd ui && yarn && popd

COPY go.mod go.sum ./
RUN go mod download && go get github.com/rakyll/statik

COPY ui ui/
RUN pushd ui && make && popd

COPY . .
RUN make

FROM scratch AS production
WORKDIR /app
COPY --from=builder /code/go-mon ./

EXPOSE 80
CMD [ "./go-mon" ]
