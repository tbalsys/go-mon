ui_dir          := ui
ui_target       := $(ui_dir)/dist
go_sources      := main.go $(wildcard server/*.go)
go_generated    := statik
go_target       := go-mon

all: ui server

ui: $(ui_target)

server: $(go_target)

$(ui_target):
	make -C $(ui_dir)

$(go_generated): $(ui_target)
	go get github.com/rakyll/statik
	go generate

$(go_target): $(go_sources) $(go_generated)
	go build -o $@ -ldflags " \
	-X gitlab.com/tbalsys/go-mon/server.buildStamp=`date -u '+%Y-%m-%d_%I:%M:%S%p'` \
	-X gitlab.com/tbalsys/go-mon/server.gitRevision=`git rev-parse HEAD`"

modules:
	make -C $(ui_dir) $@
	go mod download
	go get github.com/rakyll/statik

clean:
	make -C $(ui_dir) $@
	rm -rf $(go_target) $(go_generated)

distclean:
	make -C $(ui_dir) $@

.PHONY: all ui server modules clean distclean
