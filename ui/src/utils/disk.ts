interface DiskApiData {
    Available: number,
    Free: number,
    Size: number,
    Usage: number,
    Used: number,
}


interface DiskData {
    timestamp: number,
    available?: number,
    free?: number,
    size?: number,
    used?: number,
    usagePercent: number,
}

export class Disk {
    data: DiskData[]  = [];

    constructor(size = 120) {
        const currentTimestamp = Date.now();

        for (let i = size; i > 0; i--) {
            this.data.push({
                timestamp: currentTimestamp - 1000 * i,
                usagePercent: 0,
            });
        }
    }

    public getUsagePercent = (data: DiskApiData): [number, number][] => {
        this.addData(data);

        return this.data.map(el => [el.timestamp, el.usagePercent]);
    }

    private addData(data: DiskApiData) {
        this.data.shift();
        this.data = [...this.data, {
            timestamp: Date.now(),
            usagePercent: data.Usage * 100,
        }];
    }
}
