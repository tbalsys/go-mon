interface CpuApiData {
    Total: number,
    User: number,
    System: number,
    Idle: number,
}

interface CpuData {
    timestamp: number,
    total: number,
    user: number,
    system: number,
    idle: number,
    userPercent: number,
    systemPercent: number,
    idlePercent: number,
    usagePercent: number,
}

export class Cpu {
    data: CpuData[]  = [];

    constructor(size = 120) {
        const currentTimestamp = Date.now();

        for (let i = size; i > 0; i--) {
            this.data.push({
                timestamp: currentTimestamp - 1000 * i,
                total: 0,
                user: 0,
                system: 0,
                idle: 0,
                userPercent: 0,
                systemPercent: 0,
                idlePercent: 0,
                usagePercent: 0,
            })
        }
    }

    public getUsagePercent = (data: CpuApiData): [number, number][] => {
        this.addData(data);

        return this.data.map(el => [el.timestamp, el.usagePercent]);
    }

    private addData(data: CpuApiData) {
        const last = this.data[this.data.length - 1];
        const totalDiff = data.Total - last.total;
        this.data.shift();
        this.data = [...this.data, {
            timestamp: Date.now(),
            total: data.Total,
            user: data.User,
            system: data.System,
            idle: data.Idle,
            userPercent: (data.User - last.user) * 100 / totalDiff,
            systemPercent: (data.System - last.system) * 100 / totalDiff,
            idlePercent:  (data.Idle - last.idle) * 100 / totalDiff,
            usagePercent: (data.User + data.System - last.user - last.system) * 100 / totalDiff,
        }];
    }
}
