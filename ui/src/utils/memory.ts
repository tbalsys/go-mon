interface MemoryApiData {
    Active: number,
    Cached: number,
    Free: number,
    Inactive: number,
    SwapFree: number,
    SwapTotal: number,
    SwapUsed: number,
    Total: number,
    Used: number,
    /* 
    Total = Used + Free + Cached
    SwapTotal = SwapUsed + SwapFree
    */
}


interface MemoryData {
    timestamp: number,
    active?: number,
    cached?: number,
    free?: number,
    inactive?: number,
    swapFree?: number,
    swapTotal?: number,
    swapUsed?: number,
    total?: number,
    used?: number,
    usagePercent: number,
}

export class Memory {
    data: MemoryData[]  = [];

    constructor(size = 120) {
        const currentTimestamp = Date.now();

        for (let i = size; i > 0; i--) {
            this.data.push({
                timestamp: currentTimestamp - 1000 * i,
                usagePercent: 0,
            });
        }
    }

    public getUsagePercent = (data: MemoryApiData): [number, number][] => {
        this.addData(data);

        return this.data.map(el => [el.timestamp, el.usagePercent]);
    }

    private addData(data: MemoryApiData) {
        this.data.shift();
        this.data = [...this.data, {
            timestamp: Date.now(),
            usagePercent: data.Used * 100 / data.Total,
        }];
    }
}
