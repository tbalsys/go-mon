import * as React from "react";
import { render } from "react-dom";

import { EuiText, EuiSpacer } from "@elastic/eui";
import { StatsChart } from "./components/stats-chart";
import { StatsWebSocket } from "./components/stats-websocket";

import '@elastic/eui/dist/eui_theme_dark.css';
import '@elastic/charts/dist/theme_only_dark.css';

const App = () => (
  <div style={{ padding: 32 }}>
    <EuiText>
      <h3>Stats</h3>
    </EuiText>
    <EuiSpacer />
    <StatsWebSocket
      url={globalThis.WS_URL}
      render={({ cpuUsage, memoryUsage, diskUsage }) => (
        <>
          <StatsChart id="cpu" title="CPU Usage %" data={cpuUsage} />
          <EuiSpacer />
          <StatsChart id="memory" title="RAM Usage %" data={memoryUsage} />
          <EuiSpacer />
          <StatsChart id="disk" title="Disk Usage %" data={diskUsage} />
        </>
      )} />
  </div>
);

render(<App />, document.getElementById("app"));
