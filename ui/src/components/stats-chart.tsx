import * as React from "react";
import {
  Chart,
  Settings,
  Axis,
  ScaleType,
  AreaSeries,
  Position,
  timeFormatter,
} from '@elastic/charts';
import { EUI_CHARTS_THEME_DARK } from '@elastic/eui/dist/eui_charts_theme';

interface PropsType {
  id: string,
  title: string,
  data: [number, number][]
}

const dateFormatter = timeFormatter('HH:mm');

export const StatsChart = ({ id, title, data }: PropsType) => {
  return (
    <Chart size={{ height: 200 }} className={`${id}-chart`}>
      <Settings theme={EUI_CHARTS_THEME_DARK.theme} showLegend={false} />
      <AreaSeries
        id={`${id}-area`}
        name={title}
        data={data}
        xAccessor={0}
        yAccessors={[1]}
        xScaleType={ScaleType.Time}
        yScaleType={ScaleType.Linear} />
      <Axis
        id={`${id}-bottom`}
        title="Time"
        position={Position.Bottom}
        showOverlappingTicks
        tickFormat={dateFormatter} />
      <Axis
        id={`${id}-left`}
        title={title}
        position={Position.Left}
        tickFormat={(d) => Number(d).toFixed(0)}
        domain={{max: 100}} />
    </Chart>
  );
};
