import * as React from "react";
import { Cpu }from '../utils/cpu'
import { Memory }from '../utils/memory'
import { Disk }from '../utils/disk'

const cpuPollAction = (interval: number) => ({
    entity: 'cpu',
    action: 'poll',
    interval
});

const memoryPollAction = (interval: number) => ({
    entity: 'memory',
    action: 'poll',
    interval
});

const diskPollAction = (interval: number) => ({
    entity: 'disk',
    action: 'poll',
    interval
});

type TimeSeries = [number, number][];

interface RenderPropsType {
    cpuUsage?: TimeSeries,
    memoryUsage?: TimeSeries,
    diskUsage?: TimeSeries,
}

interface PropsType {
    url: string
    render: (props: RenderPropsType) => React.ReactElement
}

interface StateType {
    cpuUsagePercent: [number,number][],
    memoryUsagePercent: [number,number][],
    diskUsagePercent: [number,number][],
}

export class StatsWebSocket extends React.Component<PropsType, StateType> {
    private ws: WebSocket = null
    private cpu = new Cpu;
    private memory = new Memory;
    private disk = new Disk;

    public readonly state: Readonly<StateType> = {
        cpuUsagePercent: [],
        memoryUsagePercent: [],
        diskUsagePercent: [],
    };

    componentDidMount() {
        this.ws = new WebSocket(this.props.url)
        this.ws.onopen = this.open;
        this.ws.onclose = this.close;
        this.ws.onmessage = this.message;
        this.ws.onerror = this.error;
    }

    private open = () => {
        console.log("OPEN");
        this.send(cpuPollAction(1));
        this.send(memoryPollAction(2));
        this.send(diskPollAction(5));
    }

    private close = () => {
        console.log("CLOSE");
    }

    private message = (evt: MessageEvent) => {
        //console.log("RESPONSE: " + evt.data);
        const data = JSON.parse(evt.data);
        if (data.Status != 'success') {
            return;
        }

        if (data.CPU) {
            this.setState({
                cpuUsagePercent: this.cpu.getUsagePercent(data.CPU)
            })
        }
        if (data.Memory) {
            this.setState({
                memoryUsagePercent: this.memory.getUsagePercent(data.Memory)
            })
        }
        if (data.Disk) {
            this.setState({
                diskUsagePercent: this.disk.getUsagePercent(data.Disk)
            })
        }
    }

    private error = (evt: MessageEvent) => {
        console.error("ERROR: " + evt.data);
    }

    private send = (message: object) => {
        const json = JSON.stringify(message)
        this.ws.send(json);
    }

    public render() {
        const View = this.props.render;
        const { cpuUsagePercent, memoryUsagePercent, diskUsagePercent } = this.state;

        return (
            <View
                cpuUsage={cpuUsagePercent}
                memoryUsage={memoryUsagePercent}
                diskUsage={diskUsagePercent}
            />
        );
    }
}
