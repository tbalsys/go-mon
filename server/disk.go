package server

import "github.com/ricochet2200/go-disk-usage/du"

type diskUsage struct {
	Free      uint64
	Available uint64
	Size      uint64
	Used      uint64
	Usage     float32
}

type diskResponse struct {
	response
	Disk diskUsage
}

var usage = du.NewDiskUsage("/")

// GetDiskStats returns disk usage statistics
func GetDiskStats() (interface{}, error) {
	return diskResponse{
		response{statusOK},
		diskUsage{
			usage.Free(),
			usage.Available(),
			usage.Size(),
			usage.Used(),
			usage.Usage(),
		},
	}, nil
}
