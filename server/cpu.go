package server

import "github.com/mackerelio/go-osstat/cpu"

type cpuResponse struct {
	response
	CPU *cpu.Stats
}

// GetCPUStats returns CPU usage statistics
func GetCPUStats() (interface{}, error) {
	stats, err := cpu.Get()
	return cpuResponse{response{statusOK}, stats}, err
}
