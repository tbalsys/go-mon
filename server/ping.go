package server

import "time"

var (
	startTime   = time.Now()
	gitRevision = "HEAD"
	buildStamp  = "unknown"
)

type pingResponse struct {
	Uptime  string `json:"uptime"`
	Version string `json:"version"`
}

// Ping response data
func Ping() (interface{}, error) {
	return pingResponse{
		time.Since(startTime).String(),
		gitRevision + "[" + buildStamp + "]",
	}, nil
}
