package server

import (
	"log"
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/websocket"
)

const (
	statusOK                       = "success"
	statusBadCommand               = "bad-command"
	pongWait                       = 60 * time.Second
	writeWait                      = 10 * time.Second
	maxDuration      time.Duration = 1<<63 - 1
)

type successResponse struct {
	response
	Message string
}

type errorResponse struct {
	response
	Errors []string
}

type command struct {
	Entity   string        `validate:"required,oneof=cpu memory disk"`
	Action   string        `validate:"required,oneof=get poll stop"`
	Interval time.Duration `validate:"required_if=Action poll"`
}

type client struct {
	conn         *websocket.Conn
	send         chan command
	cpuTicker    *time.Ticker
	memoryTicker *time.Ticker
	diskTicker   *time.Ticker
}

var validate = validator.New()
var upgrader = websocket.Upgrader{}

func tansformValidationErrors(errors validator.ValidationErrors) []string {
	messages := make([]string, len(errors))
	for i, v := range errors {
		messages[i] = v.Error()
	}
	return messages
}

func (c *client) WriteErrors(messages []string) error {
	return c.conn.WriteJSON(errorResponse{response{statusBadCommand}, messages})
}

func (c *client) WriteError(err error) error {
	return c.WriteErrors([]string{err.Error()})
}

func (c *client) WriteSuccess(text string) error {
	return c.conn.WriteJSON(successResponse{response{statusOK}, text})
}

func (c *client) WriteStats(getStats func() (interface{}, error)) error {
	stats, err := getStats()
	if err != nil {
		return err
	}
	return c.conn.WriteJSON(stats)
}

func (c *client) readPump() {
	defer func() {
		c.conn.Close()
	}()
	c.conn.SetReadLimit(512)

	cmd := command{}

	for {
		if err := c.conn.ReadJSON(&cmd); err != nil {
			if websocket.IsUnexpectedCloseError(
				err,
				websocket.CloseGoingAway,
				websocket.CloseAbnormalClosure,
			) {
				log.Printf("connection closed: %v", err)
				break
			} else {
				if err := c.WriteError(err); err != nil {
					return
				}
			}
		} else if err := validate.Struct(cmd); err != nil {
			// this check is only needed when your code could produce
			// an invalid value for validation such as interface with nil
			// value most including myself do not usually have code like this.
			if _, ok := err.(*validator.InvalidValidationError); ok {
				if err := c.WriteError(err); err != nil {
					return
				}
			} else {
				if err := c.WriteErrors(
					tansformValidationErrors(
						err.(validator.ValidationErrors))); err != nil {
					return
				}
			}
		} else {
			c.send <- cmd
		}
	}
}

func (c *client) handleCommand(cmd *command) error {
	handleAction := func(
		getStats func() (interface{}, error),
		ticker *time.Ticker,
		pollMessage string,
		stopMessage string,
	) error {
		switch cmd.Action {
		case "get":
			c.WriteStats(getStats)
		case "poll":
			ticker.Reset(cmd.Interval * time.Second)
			return c.WriteSuccess(pollMessage)
		case "stop":
			ticker.Reset(maxDuration)
			return c.WriteSuccess(stopMessage)
		}

		return nil
	}

	switch cmd.Entity {
	case "cpu":
		return handleAction(
			GetCPUStats,
			c.cpuTicker,
			"Starting CPU polling",
			"Stopping CPU polling")
	case "memory":
		return handleAction(
			GetMemoryStats,
			c.memoryTicker,
			"Starting memory polling",
			"Stopping memory polling")
	case "disk":
		return handleAction(
			GetDiskStats,
			c.diskTicker,
			"Starting disk polling",
			"Stopping disk polling")
	}

	return nil
}

func (c *client) writePump() {
	c.cpuTicker = time.NewTicker(maxDuration)
	c.memoryTicker = time.NewTicker(maxDuration)
	c.diskTicker = time.NewTicker(maxDuration)
	defer func() {
		c.cpuTicker.Stop()
		c.memoryTicker.Stop()
		c.diskTicker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case cmd := <-c.send:
			if err := c.handleCommand(&cmd); err != nil {
				return
			}
		case <-c.cpuTicker.C:
			if err := c.WriteStats(GetCPUStats); err != nil {
				return
			}
		case <-c.memoryTicker.C:
			if err := c.WriteStats(GetMemoryStats); err != nil {
				return
			}
		case <-c.diskTicker.C:
			if err := c.WriteStats(GetDiskStats); err != nil {
				return
			}
		}
	}
}

// WsHandler is http handler which opens a websoket connection to client
func WsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	client := &client{conn: conn, send: make(chan command)}

	go client.writePump()
	go client.readPump()
}
