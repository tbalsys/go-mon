package server

import "github.com/mackerelio/go-osstat/memory"

type memoryResponse struct {
	response
	Memory *memory.Stats
}

// GetMemoryStats returns memory usage statistics
func GetMemoryStats() (interface{}, error) {
	stats, err := memory.Get()
	return memoryResponse{response{statusOK}, stats}, err
}
