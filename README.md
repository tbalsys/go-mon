# Description

Web application for monitoring system resources: CPU, Memory, Disk Usage.
Back-end is written in `Go` using WebSockets.
Front-end is written in `TypeScript` using `React` and `Elastic UI` library.

![Screen capture](docs/capture.gif)

# Build

Prerequisities: Go 1.15, Node.js 14, yarn, gcc-c++, make

```
make
```

# Run

```
./go-mon
```

And point your browser to http://localhost:8080 

# Build container image

```
podman build . --tag go-mon
```

# Run container

```
podman run --rm --publish 8080:80 go-mon
```

# Development using container

Build the development image:

```
podman build . --tag go-mon-dev --target development
```

Run the container from the root of the repository

```
podman run --detach \
    --publish 8000:80 \
    --publish 2222:22 \
    --volume ./:/root/code:Z \
    --env SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)" \
    --env SSH_PUBLIC_KEY="$(cat ~/.ssh/id_rsa.pub)" \
    --env GIT_NAME="$(git config user.name)" \
    --env GIT_EMAIL="$(git config user.email)" \
    --name go-mon-dev \
    go-mon-dev
```

This implies ssh key `~/.ssh/id_rsa.pub` is already created.

Connect to your devlopment environment:

```
ssh root@localhost -p 2222
```
